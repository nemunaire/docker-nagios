#!/usr/bin/env python

import argparse
import http.client
import json
import sys

parser = argparse.ArgumentParser(description='Nagios plugin for Matrix federation test.')
parser.add_argument('domain')
args = parser.parse_args()

conn = http.client.HTTPSConnection("federation-tester.p0m.fr")
conn.request("GET", "/api/report?server_name=" + args.domain)
tester = json.load(conn.getresponse())

msg = ""
try:
    msg += "- " + tester["Version"]["name"] + " " + tester["Version"]["version"]
except:
    pass

if tester["FederationOK"]:
    print("MATRIX FEDERATION OK " + msg)
    sys.exit(0)
elif len(tester["ConnectionReports"]):
    print("MATRIX FEDERATION WARNING " + msg)
    sys.exit(1)
else:
    print("MATRIX FEDERATION CRITICAL " + msg)
    sys.exit(2)
