FROM alpine:3.12

RUN apk add --no-cache \
    ca-certificates \
    coreutils \
    curl \
    file \
    iputils \
    mailx \
    nagios \
    nagios-plugins \
    nagios-plugins-cluster \
    nagios-plugins-dig \
    nagios-plugins-dns \
    nagios-plugins-dummy \
    nagios-plugins-http \
    nagios-plugins-ldap \
    nagios-plugins-mysql \
    nagios-plugins-pgsql \
    nagios-plugins-ping \
    nagios-plugins-smtp \
    nagios-plugins-ssh \
    nagios-plugins-ssl_validity \
    nagios-plugins-tcp \
    nagios-web \
    nrpe-plugin \
    openssh-client \
    openssl \
    perl \
    python3 \
    py3-dnspython \
    ssmtp \
    tini && \
    chown nagios /etc/ssmtp/ssmtp.conf

COPY FREE_send_notification.sh /usr/bin/FREE_send_notification.sh
COPY entrypoint.sh /docker-entrypoint.sh

ADD https://raw.githubusercontent.com/stump/check_sshfp/master/check_sshfp /usr/lib/nagios/plugins/check_sshfp
ADD https://raw.githubusercontent.com/matteocorti/check_ssl_cert/master/check_ssl_cert /usr/lib/nagios/plugins/check_ssl_cert
ADD https://gist.githubusercontent.com/nemunaire/9669659/raw/977cfce7246249c17e213b0b2b231220257754fa/nagios-status.pl /usr/bin/nagios-status.pl
RUN chmod 755 /usr/lib/nagios/plugins/check_sshfp /usr/lib/nagios/plugins/check_ssl_cert /usr/bin/nagios-status.pl
COPY check_matrix.py /usr/lib/nagios/plugins/check_matrix

ENTRYPOINT ["/docker-entrypoint.sh"]
USER nagios
VOLUME ["/etc/nagios", "/var/nagios"]
CMD ["/usr/sbin/nagios", "/etc/nagios/nagios.cfg"]
