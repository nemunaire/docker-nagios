#!/bin/sh

[ -n "${MAILER_HOST}" ] && sed -r "/^mailhub=/s/.*/mailhub=${MAILER_HOST}/" /etc/ssmtp/ssmtp.conf > ~/ssmtp.conf && cat ~/ssmtp.conf > /etc/ssmtp/ssmtp.conf
[ -n "${MAILER_REWRITEDOMAIN}" ] && sed -r "/^#?rewriteDomain=/s/.*/rewriteDomain=${MAILER_REWRITEDOMAIN}/" /etc/ssmtp/ssmtp.conf > ~/ssmtp.conf && cat ~/ssmtp.conf > /etc/ssmtp/ssmtp.conf

exec /sbin/tini -- $@
