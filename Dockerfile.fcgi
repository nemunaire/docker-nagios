FROM alpine:3.12

RUN adduser -S -s /sbin/nologin -h /var/www/localhost/htdocs -u 80 www-data && \
    apk add --no-cache \
      lighttpd \
      nagios \
      nagios-web \
    && \
    addgroup lighttpd nagios

COPY lighttpd.conf /etc/lighttpd/lighttpd.conf

EXPOSE 80
USER lighttpd
CMD ["/usr/sbin/lighttpd", "-f", "/etc/lighttpd/lighttpd.conf", "-D"]
